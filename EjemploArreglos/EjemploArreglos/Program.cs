﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemploArreglos
{
    class Program
    {
        static void Main(string[] args)
        {
            int suma = 0;
            int[] catalogoNumeros = new int[10];//Declaracion de Arreglo

            for(int i=0;i<catalogoNumeros.Length;i++)
            {
                Console.WriteLine("Ingresa Un Numero");
                int num = Convert.ToInt32(Console.ReadLine());
                catalogoNumeros[i] = num;
            }

            Console.WriteLine("");

            foreach(int val in catalogoNumeros)
            {
                Console.WriteLine(val);
                suma += val;
            }
            Console.WriteLine("\n" + suma);

            Console.ReadKey();
        }
    }
}
