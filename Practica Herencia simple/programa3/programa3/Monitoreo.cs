﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace programa3
{
    class Monitoreo
    {
        public string nro_monitoreo = "130";
        public string diagnostico_pac = "Besicula inflamada";
        public string hora = "5:00am";
        public string pulso = "86lpm";
        public string presion = "80";
        public string nro_contracciones = "5";
        public string hem_cantidad = "5";
        public string convulciones_hora = "1 c/h";
        public string paro_cardiaco = "NO";
        public string otros = "NO";
    }
}
