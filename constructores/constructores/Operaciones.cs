﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace constructores
{
    class Operaciones
    {
        private int multa = 0;
        public Operaciones(int multa)
        {
            this.multa = multa;
        }

        public int CalcularMulta()
        {
            int resultado = 0;
            switch (this.multa)
            {
                case 0:
                    resultado = 180;
                    break;
                case 1:
                    resultado = 500;
                    break;
                case 2:
                    resultado = 300;
                    break;
                case 3:
                    resultado = 1000;
                    break;
                default:
                    resultado = 0;
                    break;
            }
            return resultado;
        }
    }
}
