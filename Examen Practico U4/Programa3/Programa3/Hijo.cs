﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programa3
{
    class Hijo : Interfaz
    {
        public void Metodo1()
        {
            int po;
            int can;
            int res;
            Console.WriteLine("Calculo de porcentajes");
            Console.WriteLine("Introduce el porcentaje a calcular");
            po = int.Parse(Console.ReadLine());
            Console.WriteLine("Introduce el numero a sacar el porcentaje");
            can = int.Parse(Console.ReadLine());
            res = (po * 100)/can;
            Console.WriteLine("El resultado es: " + res + "%");
        }
        public void Metodo2()
        {
            Console.WriteLine("Hola metodo 2 desde clase Hija");
        }
    }
}
