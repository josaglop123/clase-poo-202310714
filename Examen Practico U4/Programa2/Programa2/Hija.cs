﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programa2
{
    class Hija:Padre
    {
        public override void Metodo()
        {
            Console.WriteLine("Hola desde la clase Hija por el metodo abstracto");
        }
        public void Acceso()
        {
            Metodo2();
        }
    }
}
