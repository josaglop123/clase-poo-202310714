﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programa2
{
    abstract class Padre
    {
        public abstract void Metodo();
        
        public void Metodo2()
        {
            Console.WriteLine("Arreglo");
            int[] ar= new int[3];
            for(int i=0;i<3;i++)
            {
                ar[i] = 2;
            }
            for(int i=0;i<3;i++)
            {
                Console.WriteLine(ar[i]);
            }
        }
    }
}
