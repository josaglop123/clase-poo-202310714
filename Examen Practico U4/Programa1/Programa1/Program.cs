﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programa1
{
    class Program
    {
        static void Main(string[] args)
        {
            int a;
            int b;
            double c;
            double d;
            Operaciones obj = new Operaciones();

            Console.WriteLine("Introduce las calificaciones base 100");
            a = int.Parse(Console.ReadLine());
            b = int.Parse(Console.ReadLine());

            Console.WriteLine("\nIntroduce las calificaciones base 10");
            c = double.Parse(Console.ReadLine());
            d = double.Parse(Console.ReadLine());

       
            obj.Calificaciones(a, b);
            obj.Calificaciones(c, d);
            obj.Aprobacion(a, b);
            obj.Aprobacion(c, d);

            Console.ReadKey();
        }
    }
}
