﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programa1
{
    class Operaciones
    {
        public void Calificaciones(int a,int b)
        {
            int s = (a + b)/2;
            Console.WriteLine("La calificacion final base 100 es: " + s);
        }

        public void Calificaciones(double a, double b)
        {
            double s = (a + b)/2;
            Console.WriteLine("La calificacion final base 10 es: " + s );
        }

        public void Aprobacion(int a, int b)
        {
            int s = (a + b)/2;
            if(s>=70)
            {
                Console.WriteLine("Base 100 Aprobado");
            }
            else
            {
                Console.WriteLine("Base 100 Reprobado");
            }
        }

        public void Aprobacion(double a, double b)
        {
            double s = (a + b) / 2;
            if (s >= 7)
            {
                Console.WriteLine("Base 10 Aprobado");
            }
            else
            {
                Console.WriteLine("Base 10 Reprobado");
            }
        }
    }
}
