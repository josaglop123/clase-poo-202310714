﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programa1
{
    class EmpleadoPorHoras: Empleado
    {
        public double sueldo;
        public int horas;

        public override void CalcularSalario()
        {
            Console.WriteLine("Introduce las Horas trabajadas");
            horas = int.Parse(Console.ReadLine());
            Console.WriteLine("Introduce el sueldo por horas");
            sueldo = double.Parse(Console.ReadLine());
            if(horas<=40)
            {
                sueldo = sueldo * horas;
            }
            else if(horas>40)
            {
                sueldo = (40 * horas) * sueldo * 1.5;
            }
           
        }
        public override void Datos()
        {
            Console.WriteLine("Introduce el nombre del empleado por horas");
            primerNombre = Console.ReadLine();
            Console.WriteLine("Introduce el apellido paterno del empleado por horas");
            apellidoPat = Console.ReadLine();
            Console.WriteLine("Introduce el Numero de seguridad social del empleado por horas");
            nss = Console.ReadLine();
            Console.WriteLine("El sueldo es de " + sueldo);
        }
    }
}
