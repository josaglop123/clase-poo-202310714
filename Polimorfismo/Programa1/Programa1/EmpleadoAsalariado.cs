﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programa1
{
    class EmpleadoAsalariado : Empleado
    {
        public int salariosemanal;
        
        public override void CalcularSalario()
        {
            Console.WriteLine("Introduce el sueldo del empleado");
            salariosemanal = int.Parse(Console.ReadLine());
        }
        public override void Datos()
        {
            Console.WriteLine("Introduce el nombre del empleado asalariado");
            primerNombre = Console.ReadLine();
            Console.WriteLine("Introduce el apellido paterno del empleado Asalariado");
            apellidoPat = Console.ReadLine();
            Console.WriteLine("Introduce el Numero de seguridad social del empleado asalariado");
            nss = Console.ReadLine();
            Console.WriteLine("EL sueldo es "+salariosemanal);
        }
    }
}
