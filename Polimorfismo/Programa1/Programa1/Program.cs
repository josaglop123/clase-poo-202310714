﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programa1
{
    class Program
    {
        static void Main(string[] args)
        {
            EmpleadoAsalariado obj1 = new EmpleadoAsalariado();
            EmpleadoPorHoras obj2 = new EmpleadoPorHoras();
            EmpleadoPorComision obj3 = new EmpleadoPorComision();
            BaseMasComision obj4 = new BaseMasComision();
            obj1.CalcularSalario();
            obj1.Datos();
            obj2.CalcularSalario();
            obj2.Datos();
            obj3.CalcularSalario();
            obj3.Datos();
            obj4.CalcularSalario();
            obj4.Datos();
            Console.ReadKey();
        }
    }
}
