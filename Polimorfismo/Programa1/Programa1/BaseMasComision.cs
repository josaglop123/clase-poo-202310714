﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programa1
{
    class BaseMasComision:Empleado
    {
        public int sbase;
        public int ventasBrutas;
        public int tarifaComision;
        public int salario;
        public override void CalcularSalario()
        {
            Console.WriteLine("Introduce las ventas brutas");
            ventasBrutas = int.Parse(Console.ReadLine());
            Console.WriteLine("Introduce la tarifa de comision");
            tarifaComision = int.Parse(Console.ReadLine());
            Console.WriteLine("Introduce el salario base");
            sbase = int.Parse(Console.ReadLine());
            salario = (tarifaComision * ventasBrutas) + sbase;
        }
        public override void Datos()
        {
            Console.WriteLine("Introduce el nombre del empleado de salario base mas comision");
            primerNombre = Console.ReadLine();
            Console.WriteLine("Introduce el apellido paterno del empleado de salario base mas comision");
            apellidoPat = Console.ReadLine();
            Console.WriteLine("Introduce el Numero de seguridad social del empleado de salario base mas comision");
            nss = Console.ReadLine();
            Console.WriteLine("El sueldo es de " + salario);
        }
    }
}
