﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programa1
{
    class EmpleadoPorComision :Empleado
    {
        public int ventasBrutas;
        public int tarifaComision;
        public int salario;

        public override void CalcularSalario()
        {
            Console.WriteLine("Introduce las ventas brutas");
            ventasBrutas = int.Parse(Console.ReadLine());
            Console.WriteLine("Introduce la tarifa de comision");
            tarifaComision = int.Parse(Console.ReadLine());
            salario = tarifaComision * ventasBrutas;
        }
        public override void Datos()
        {
            Console.WriteLine("Introduce el nombre del empleado por comision");
            primerNombre = Console.ReadLine();
            Console.WriteLine("Introduce el apellido paterno del empleado por comision");
            apellidoPat = Console.ReadLine();
            Console.WriteLine("Introduce el Numero de seguridad social del empleado por comision");
            nss = Console.ReadLine();
            Console.WriteLine("El sueldo es de " + salario);
        }
    }
}
