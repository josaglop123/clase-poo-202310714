﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace prog3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            operaciones obj = new operaciones();
            try
            {
                obj.n1 = int.Parse(txtNumu.Text);
                obj.n2 = int.Parse(txtNumd.Text);
                obj.Op();
                lblRes.Text = "" + obj.res;
            }
            catch(DivideByZeroException)
            {
                MessageBox.Show("No se puede dividir entre 0");
            }
            
        }
    }
}
