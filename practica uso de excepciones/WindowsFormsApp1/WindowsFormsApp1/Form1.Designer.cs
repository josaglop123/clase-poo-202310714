﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPres = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnPres
            // 
            this.btnPres.Location = new System.Drawing.Point(160, 148);
            this.btnPres.Name = "btnPres";
            this.btnPres.Size = new System.Drawing.Size(75, 23);
            this.btnPres.TabIndex = 0;
            this.btnPres.Text = "Presiona";
            this.btnPres.UseVisualStyleBackColor = true;
            this.btnPres.Click += new System.EventHandler(this.btnPres_Click);
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(387, 356);
            this.Controls.Add(this.btnPres);
            this.Name = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnPresiona;
        private System.Windows.Forms.Button btnPres;
    }
}

