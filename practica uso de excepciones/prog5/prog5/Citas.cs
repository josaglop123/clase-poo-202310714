﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog5
{
    class Citas
    {
        public string dia;
        public string hora;
        public string minuto;
        public string descripcion;

        public Citas(string dia, string descr, string hora, string minuto )
        {
            this.dia = dia;
            this.descripcion = descr;
            this.hora = hora;
            this.minuto = minuto;
        }

    }
}
