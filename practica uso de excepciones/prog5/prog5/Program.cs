﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog5
{
    class Program
    {
        static void Main(string[] args)
        {
            string dia;
            string hora;
            string minuto;
            string descripcion;
            try
            {
                Console.WriteLine("Escribe el dia de la cita");
                dia = Console.ReadLine();
                Console.WriteLine("Escribe la hora de la cita");
                hora = Console.ReadLine();
                Console.WriteLine("Escribe el minuto de la cita");
                minuto = Console.ReadLine();
                Console.WriteLine("Escribe la Descripcion de la cita");
                descripcion = Console.ReadLine();
                Citas obj = new Citas(dia, hora, minuto, descripcion);

            }
            catch(ArgumentException e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                Console.WriteLine("Introdujiste un valor erroneo");
            }

        }
    }
}
