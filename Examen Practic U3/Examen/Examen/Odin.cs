﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen
{
    class Odin
    {
        private string Gungnir;

        public Odin()
        {
        }

        public void Racnarok()
        { 
            Console.WriteLine("Fin de las tierras asgardianas");
        }

        private void Sleipnir()
        {
            Gungnir = "Mi Gran Lanza";
            Console.WriteLine("Imponente caballo y " + Gungnir);
        }

        protected void Asgard()
        {
            Console.WriteLine("Mi reino ahora es tuyo");
        }

        public void AccesoSleipnir()
        {
            Sleipnir();
        }

        ~Odin()
        {
            Console.WriteLine("Out..");
        }
    }


    class Thor : Odin
    {
        public Thor()
        {
        }

        public void AccesoAsgard()
        {
            Asgard();
        }
        ~Thor()
        {
            Console.WriteLine("Out..");
        }
    }
}

