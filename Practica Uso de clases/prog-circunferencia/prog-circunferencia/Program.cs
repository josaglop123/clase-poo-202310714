﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_circunferencia
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("AREA Y PERIMETRO DE UNA CIRCUNFERENCIA\n");

            circunferencia rueda = new circunferencia();
            Console.WriteLine("RUEDA");
            rueda.setRadio(10);
            double rRrueda = rueda.getRadio();

            rueda.area();
            rueda.perimetro();

            circunferencia moneda = new circunferencia();
            Console.WriteLine("\nMONEDA");
            moneda.setRadio(1);
            double rMoneda = moneda.getRadio();

            moneda.area();
            moneda.perimetro();

            Console.ReadKey();

        }
    }
}
