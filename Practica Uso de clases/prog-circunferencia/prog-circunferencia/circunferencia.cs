﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_circunferencia
{
    class circunferencia
    {
        private double radio = 0.0;

        public void setRadio(double radio)//set sin retorno de valores
        {
            this.radio = radio;
        }

        public double getRadio()//get retorna un valor del tipo solicitado 
        {
            return this.radio;
        }

        public void area()
        {
            Console.WriteLine("El area es =" + (System.Math.PI * (radio * radio)));
        }

        public void perimetro()
        {
            Console.WriteLine("El perimetro es = " + (2 * System.Math.PI * radio));
        }
    }
}
