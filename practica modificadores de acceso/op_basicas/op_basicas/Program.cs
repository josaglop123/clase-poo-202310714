﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace op_basicas
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b;
            OperacionesAritméticas oper = new OperacionesAritméticas();
            Console.WriteLine("Teclea 2 numeros:");
            a = Convert.ToInt32(Console.ReadLine());
            b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("La suma es: " + oper.suma(a, b));
            Console.WriteLine("La resta es: " + oper.resta(a, b));
            Console.WriteLine("La multiplicacion es: " + oper.multiplica(a, b));
            Console.WriteLine("La division es: " + oper.Divide(a, b));
            Console.ReadKey();
        }
    }
}
