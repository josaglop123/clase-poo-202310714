﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace op_basicas
{
    class OperacionesAritméticas
    {
        public double suma(double a, double b)
        {
            return a + b;
        }
        public double resta(double a, double b)
        {
            return a - b;
        }
        public double multiplica(double a, double b)
        {
            return a * b;
        }
        public double Divide(double a, double b)
        {
            return a / b;
        }
    }
}
