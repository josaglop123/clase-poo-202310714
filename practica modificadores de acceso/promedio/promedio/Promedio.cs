﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace promedio
{
    class Promedio
    {
        private int cal1;
        private int cal2;
        private int cal3;
        private double promedio;

        public void CalcularPromedio()
        {
            Console.WriteLine("Introduce una calificacion");
            cal1 = int.Parse(Console.ReadLine());
            Console.WriteLine("\nIntroduce una calificacion");
            cal2 = int.Parse(Console.ReadLine());
            Console.WriteLine("\nIntroduce una calificacion");
            cal3 = int.Parse(Console.ReadLine());
            promedio = (cal1 + cal2 + cal3) / 3;
        }

        public void MostrarPromedio()
        {
            Console.WriteLine("\nEl promedio de las tres calificaciones es: " + promedio);
        }
    }
}
