﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matriz
{
    class Matriz
    {
        private int[,] a=new int[5,4];


        public void LeeDatos()
        {
            Console.WriteLine("\nLlenado de la Matriz");
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Console.WriteLine("Introduce el valor de: [" + i + "," + j + "]");
                    a[i,j] = int.Parse(Console.ReadLine());
                }
            }
        }

        public void MostrarDatos()
        {
            Console.WriteLine("\n Matriz\n");
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Console.Write(a[i, j]);
                }
                Console.WriteLine();
            }
        }
    }
}
