﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Area
{
    class Operaciones
    {
        private int Base;
        private int altura;
        private double Area;

        public void Calcular()
        {
            Console.WriteLine("Introduce el valor de la base: ");
            Base = int.Parse(Console.ReadLine());
            Console.WriteLine("Introduce el valor de la altura: ");
            altura = int.Parse(Console.ReadLine());
            Area = (Base * altura) / 2;
        }

        public void Mostrar()
        {
            Console.WriteLine("El Area del triangulo es: " + Area);
        }
    }
}
