﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace edad
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalcula_Click(object sender, EventArgs e)
        {
            int a;
            a = Int32.Parse(txtAños.Text);
            Calcula(a);
        }
        void Calcula(int b)
        {
            int edad = Int32.Parse(txtAños.Text);
            edad = edad * 12;
            lblMeses.Text = edad.ToString();
        }
    }
}
