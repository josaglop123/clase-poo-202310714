﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vector
{
    class Vector
    {
        private int[] a = new int[20];


        public void LeeDatos()
        {
            Console.WriteLine("Introduce los valores del Vector\n");
            for(int i=0;i<a.Length;i++)
            {
                Console.WriteLine("Valor:" + i);
                a[i] = int.Parse (Console.ReadLine());
            }
        }

        public void MostrarDatos()
        {
            Console.WriteLine("\nEl vector tiene los siguientes valores\n");
            foreach(int val in a)
            {
                Console.Write(" " + val);
            }
        }
    }
}
