﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaMatriz
{
    class Operacion
    {
        private int M;
        private int N;
        int[,] matriz;

        public void LeerMatriz()
        { 
            Console.Write("Filas: ");
            N = int.Parse(Console.ReadLine());
            Console.Write("\nColumnas: ");
            M = int.Parse(Console.ReadLine());
            matriz = new int[N, M];
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < M; j++)
                {
                    Console.Write("Ingresa valor [" + i + "," + j + "]= ");
                    matriz[i,j] = int.Parse(Console.ReadLine());
                }
            }
        }

        public void ImprimeMatriz()
        {
            Console.WriteLine("\nMATRIZ\n");
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < M; j++)
                {
                    Console.Write(matriz[i,j] + " ");
                }
                Console.WriteLine();
            }
        }
    }
}
