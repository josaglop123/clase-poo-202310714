﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arreglo
{
    class Operacion
    {
        private int suma = 0;
        private int[] n = new int[5];
        public void ImprimePares()
        {
            for(int i=0;i<n.Length;i++)
            {
                Console.WriteLine("Ingresa Un Numero");
                int num = Convert.ToInt32(Console.ReadLine());
                n[i] = num;
            }
            Console.WriteLine("\nNUMEROS PARES\n");
            for (int i = 0; i < n.Length; i++)
            {
                if (n[i] % 2 == 0)
                {
                    Console.Write(" "+n[i]);
                }
            }
        }

        public void SumaArreglo()
        {
            foreach (int val in n)
            {
                suma += val;
            }
            Console.WriteLine("\n\nLa suma de los valores es:\n" + suma);
        }
    }
}
