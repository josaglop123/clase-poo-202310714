﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace programa6
{
    class Operaciones
    {
        public void Vectores(short[] a)
        {
            int max = 0;
            for(int i=0;i<20;i++)
            {
                if (a[i] > max)
                {
                    max = a[i];
                }
            }
            Console.WriteLine("El valor maximo del arreglo short es: " + max);
        }
        public void Vectores(long[] a)
        {
            long max = 0;
            for (int i = 0; i < 20; i++)
            {
                if (a[i] > max)
                {
                    max = a[i];
                }
            }
            Console.WriteLine("El valor maximo del arreglo Long es: " + max);
        }
    }
}
