﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace programa6
{
    class Program
    {
        static void Main(string[] args)
        {
            short[] a = new short[20];
            long[] b = new long[20];
            Operaciones obj = new Operaciones();

            Console.WriteLine("Elementos del arreglo short:");
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = short.Parse(Console.ReadLine());
            }
            Console.WriteLine("Elementos del arreglo long:");
            for (int i = 0; i < b.Length; i++)
            {
                b[i] = long.Parse(Console.ReadLine());
            }

            obj.Vectores(a);
            obj.Vectores(b);
            Console.ReadKey();
        }
    }
}
