﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace programa7
{
    class Operaciones
    {
        public void comparacion(int[] a)
        {
            int positivo = 0;
            int negativo = 0;
            int cero = 0;
            int ap = 0;
            int an = 0;
            for(int i=0;i<300;i++)
            {
                if(a[i]<0)
                {
                    negativo++;
                    an += a[i];
                }
                else
                {
                    if(a[i]>0)
                    {
                        positivo++;
                        ap += a[i];
                    }
                    else
                    {
                        cero++;
                    }
                }
            }
            Console.WriteLine("Hay " + negativo + " numeros negativos");
            Console.WriteLine("La suma de los negativos es: " + an);
            Console.WriteLine("Hay " + positivo + " numeros Positivos");
            Console.WriteLine("La suma de los positivos es: " + ap);
            Console.WriteLine("Hay " + cero + " Ceros");
        }
    }
}
