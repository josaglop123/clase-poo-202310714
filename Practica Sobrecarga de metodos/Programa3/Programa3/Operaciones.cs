﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programa3
{
    class Operaciones
    {
        public int ElevarCuadrado(int a)
        {
            int s = a*a;
            return s;
        }

        public double ElevarCuadrado(double b)
        {
            double s = b*b;
            return s;
        }

    }
}
