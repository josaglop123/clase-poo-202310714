﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programa3
{
    class Program
    {
        static void Main(string[] args)
        {
            Operaciones obj = new Operaciones();
            Console.WriteLine("Variable tipo entera elevada al cuadrado: " + obj.ElevarCuadrado(5));
            Console.WriteLine("Variable tipo Double elevada al cuadrado: " + obj.ElevarCuadrado(5.5));
            Console.ReadKey();
        }
    }
}
