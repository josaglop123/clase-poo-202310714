﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace programa_2
{
    class Operaciones
    {
        public int Sumar(int a1, int a2)
        {
            int s = a1 + a2;
            return s;
        }

        public int Sumar(int b1, int b2, int b3)
        {
            int s = b1 + b2 + b3;
            return s;
        }

        public int Sumar(int c1, int c2, int c3, int c4)
        {
            int s = c1 + c2 + c3 + c4;
            return s;
        }

    }
}
