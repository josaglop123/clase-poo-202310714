﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace programa_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Operaciones obj = new Operaciones();

            Console.WriteLine("La suma de dos variables es: " + obj.Sumar(10, 4));
            Console.WriteLine("La suma de tres variables es: " + obj.Sumar(15, 35, 23));
            Console.WriteLine("La suma de cuatro variables es: " + obj.Sumar(10, 12, 12, 18));

            Console.ReadKey();
        }
    }
}
