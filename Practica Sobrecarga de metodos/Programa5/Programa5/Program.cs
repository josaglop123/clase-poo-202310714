﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programa5
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = new int[5];
            double[] b = new double[5];
            Operaciones obj = new Operaciones();

            Console.WriteLine("Elementos del arreglo entero:");
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("Elementos del arreglo decimales:");
            for (int j = 0; j < b.Length; j++)
            {
                b[j] = double.Parse(Console.ReadLine());
            }
            obj.Burbuja(a);
            obj.Burbuja(b);
            Console.ReadLine();
        }
    }
}
