﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programa5
{
    class Operaciones
    {
        public void Burbuja(int[] a)
        {
            int T;
            for (int i = 0; i <= 5; i++)
            {
                for (int j = 4; j >= i; j--)
                {
                    if (a[j-1] > a[j])
                    {
                        T = a[j-1];
                        a[j-1] = a [j];
                        a[j] = T;
                    }
                }

            }
            Console.WriteLine("\nVector ordenados en forma ascendente\n");
            for (int f = 0; f < a.Length; f++)
            {
                Console.Write(a[f] + "  ");
            }
        }

        public void Burbuja(double[] a)
        {
            double T;
            for (int i = 0; i <= 5; i++)
            {
                for (int j = 4; j >= i; j--)
                {
                    if (a[j-1] > a[j])
                    {
                        T = a[j-1];
                        a[j-1] = a[j];
                        a[j] = T;
                    }
                }

            }
            Console.WriteLine("\nVector ordenados en forma ascendente");
            for (int f = 0; f < a.Length; f++)
            {
                Console.Write(a[f] + "  ");
            }
        }
    }
}
