﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstracta
{
    class Auto: Transporte
    {
        public override void Mantenimiento()
        {
            Console.WriteLine("Listado de mantenimiento de auto ");
            Console.WriteLine("\n-Cambio de Aceite\n");
            Console.WriteLine("\n-Cambio de Bujias\n");
            Console.WriteLine("\n-Chequeo de llantas\n");
            Console.WriteLine("\n-Liquido de frenos\n");
            Console.WriteLine("\n-Cambio de filtros\n");
        }
    }
}
