﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstracta
{
    class Avion: Transporte
    {   
        public override void Mantenimiento()
        {
            Console.WriteLine("\nListado de mantenimiento de Avion ");
            Console.WriteLine("\n-Alas\n");
            Console.WriteLine("\n-Pico\n");
            Console.WriteLine("\n-Motor\n");
            Console.WriteLine("\n-Llantas\n");
        }
    }
}
