﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstracta
{
    class Program
    {
        static void Main(string[] args)
        {
            Avion obj = new Avion();
            Auto obj2 = new Auto();
            obj.Mantenimiento();
            obj2.Mantenimiento();
            Console.ReadKey();
        }
    }
}
