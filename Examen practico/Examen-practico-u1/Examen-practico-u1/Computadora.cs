﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen_practico_u1
{
    class Computadora
    {
        //Propiedades
        private string motherboard = "";
        private string procesador = "";
        private int ram = 0;
        private string gabinete = "";
        private int almacenamiento = 0;
        private int capfuente = 0;
        private string grafica = "";

        //Metodos Motherboard
        public void setMotherboard(string motherboard)//set sin retorno de valores
        {
            this.motherboard = motherboard;//this hace referencia directa a una  propiedad
        }

        public string getMotherboard()//get retorna un valor del tipo solicitado (string)
        {
            return this.motherboard;
        }

        //Metodos Procesador
        public void setProcesador(string procesador)//set sin retorno de valores
        {
            this.procesador = procesador;
        }

        public string getProcesador()//get retorna un valor del tipo solicitado (strig)
        {
            return this.procesador;
        }

        //Metodos Ram
        public void setRam(int ram)
        {
            this.ram = ram;
        }

        public int getRam()
        {
            return this.ram;
        }

        //Metodos Gabinete
        public void setGabinete(string gabinete)
        {
            this.gabinete = gabinete;
        }

        public string getGabinete()
        {
            return this.gabinete;
        }

        //Metodos Almacenamiento
        public void setAlmacenamiento(int almace)
        {
            this.almacenamiento = almace;
        }

        public int getAlmacenamiento()
        {
            return this.almacenamiento;
        }

        //Metodos Capacidad de Fuente
        public void setCapfuente(int capfuente)
        {
            this.capfuente = capfuente;
        }

        public int getCapfuente()
        {
            return this.capfuente;
        }

        //Metodos Capacidad de Fuente
        public void setGrafica(string grafica)
        {
            this.grafica = grafica;
        }

        public string getGrafica()
        {
            return this.grafica;
        }
    }
}
