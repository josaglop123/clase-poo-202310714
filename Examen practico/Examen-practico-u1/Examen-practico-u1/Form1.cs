﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Examen_practico_u1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnClase_Click(object sender, EventArgs e)
        {
            Computadora CPU1 = new Computadora();//declaracion de un objeto accediendo a la clase

            //llamar metodos set y get
            CPU1.setMotherboard("Asus ATX ROG STRIX B450-F GAMING");//Motherboard
            string MotherCpu1 = CPU1.getMotherboard();

            CPU1.setProcesador("AMD RYZEN 5");//Procesador
            string ProcesadorCpu1  = CPU1.getProcesador();

            CPU1.setRam(16);//Ram
            int RamCpu1 = CPU1.getRam();

            CPU1.setGabinete("Yeyian SHADOW 2200 Full Tower");//Gabinete
            string GabineteCpu1 = CPU1.getGabinete();

            CPU1.setAlmacenamiento(1);//Almacenamiento
            int AlmacCpu1 = CPU1.getAlmacenamiento();

            CPU1.setCapfuente(600);//Capacidad de Fuente
            int CapfueCpu1 = CPU1.getCapfuente();

            CPU1.setGrafica("GIGABYTE NVIDIA GEFORCE GTX 1050Ti");//Grafica
            string GraficaCpu1 = CPU1.getGrafica();

            //Mensaje que muestra los valores 
            MessageBox.Show("La tarjeta madre es una: " + MotherCpu1);
            MessageBox.Show("El procesador es un: " + ProcesadorCpu1);
            MessageBox.Show("LA ram es de" + RamCpu1.ToString() + "GB");
            MessageBox.Show("El gabinete es un: " + GabineteCpu1);
            MessageBox.Show("La capacidad de almacenamiento es: " + AlmacCpu1.ToString() + "TRB");
            MessageBox.Show("La capacidad de la fuente de poder es de: " + CapfueCpu1.ToString() + "Watts");
            MessageBox.Show("La tarjeta grafica es una: " + GraficaCpu1);
        }
    }
}
