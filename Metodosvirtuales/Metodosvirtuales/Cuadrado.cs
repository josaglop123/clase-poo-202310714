﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metodosvirtuales
{
    class Cuadrado: Triangulo
    {
        public override void CalcularArea(double b, double a)
        {
            double resultado = b * a;
            Console.WriteLine("El area del cuadrado es: " + resultado);
        }
    }
}
