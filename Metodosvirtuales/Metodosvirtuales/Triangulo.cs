﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metodosvirtuales
{
    class Triangulo
    {
        public virtual void CalcularArea(double b,double a)
        {
            double resultado = (b * a) / 2;
            Console.WriteLine("El resultado del area del triangulo es: " + resultado);
        }
    }
}
