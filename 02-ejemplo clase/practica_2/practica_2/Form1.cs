﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace practica_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnClase_Click(object sender, EventArgs e)
        {
            tv TV1 = new tv();//declaracion de un objeto accediendo a la clase

            //llamar metodos set y get
            TV1.setTamanio(30);//Tamanio
            int tamanioTv1 = TV1.getTamanio();

            TV1.setVolumen(20);//Volumen
            int volumenTv1 = TV1.getVolumen();

            TV1.setColor("Gris");//Color
            string colorTv1 = TV1.getColor();

            TV1.setBrillo(80);//Brillo
            int brilloTv1 = TV1.getBrillo();

            TV1.setContraste(70);//Contraste
            int contrasteTv1 = TV1.getContraste();

            TV1.setMarca("Samsung");//Marca
            string marcaTv1 = TV1.getMarca();

            //Mensaje que muestra los valores 
            MessageBox.Show("El tamaño de la Tv1 es: " + tamanioTv1.ToString() + " Pulgadas");
            MessageBox.Show("El volumen de la Tv1 es: " + volumenTv1.ToString());
            MessageBox.Show("El color de la Tv1 es: " + colorTv1);
            MessageBox.Show("El brillo de la Tv1 es: " + brilloTv1.ToString());
            MessageBox.Show("El contraste de la Tv1 es: " + contrasteTv1.ToString());
            MessageBox.Show("El Marca de la Tv1 es: " + marcaTv1);

        }
    }
}
