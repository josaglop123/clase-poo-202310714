﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practica_2
{
    class tv
    {
        //Propiedades
        private int tamanio = 0;
        private int volumen = 0;
        private string color = "";
        private int brillo = 0;
        private int contraste = 0;
        private string marca = "";

        //Metodos Tamanio
        public void setTamanio(int tamanio)//set sin retorno de valores
        {
            this.tamanio = tamanio;//this hace referencia directa a una  propiedad
        }

        public int getTamanio()//get retorna un valor del tipo solicitado (int)
        {
            return this.tamanio;
        }

        //Metodos Volumen
        public void setVolumen(int volumen)//set sin retorno de valores
        {
            this.volumen = volumen;
        }

        public int getVolumen()//get retorna un valor del tipo solicitado (int)
        {
            return this.volumen;
        }

        //Metodos Color
        public void setColor(string color)
        {
            this.color = color;
        }

        public string getColor()
        {
            return this.color;
        }

        //Metodos Brillo
        public void setBrillo(int brillo)
        {
            this.brillo = brillo;
        }

        public int getBrillo()
        {
            return this.brillo;
        }
        //Metodos Contraste
        public void setContraste(int contraste)
        {
            this.contraste = contraste;
        }

        public int getContraste ()
        {
            return this.contraste;
        }

        //Metodos Marca
        public void setMarca(string marca)
        {
            this.marca = marca;
        }

        public string getMarca()
        {
            return this.marca;
        }
    }
}
