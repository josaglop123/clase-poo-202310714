﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace programa_1
{
    class Empleado
    {
        public int salario = 0;
        public int ht = 0;
        public int he = 0;
        public virtual void CalculaSalario()
        {

        }
    }
    class Intendente:Empleado
    {
        public override void CalculaSalario()
        {
            Console.WriteLine("Intendente");
            Console.WriteLine("Introduce las Horas del turno trabajadas");
            ht = int.Parse(Console.ReadLine());
            Console.WriteLine("Introduce las Horas Extras trabajadas trabajadas");
            he = int.Parse(Console.ReadLine());
            ht = ht * 20;
            he = he * 20;
            salario = he + ht;
            Console.WriteLine("EL Salaro del intendente es de $" + salario);
            Console.WriteLine();
        }
    }
    class Produccion:Empleado
    {
        public override void CalculaSalario()
        {
            Console.WriteLine("\nProduccion");
            Console.WriteLine("Introduce las Horas del turno trabajadas");
            ht = int.Parse(Console.ReadLine());
            Console.WriteLine("Introduce las Horas Extras trabajadas trabajadas");
            he = int.Parse(Console.ReadLine());
            ht = ht * 35;
            he = he * 35;
            salario = he + ht;
            Console.WriteLine("EL Salaro del Empleado de produccion es de $" + salario);
            Console.WriteLine();
        }
    }
    class Administrativos:Empleado
    {
        public override void CalculaSalario()
        {
            Console.WriteLine("\nAdministrativos");
            Console.WriteLine("Introduce las Horas del turno trabajadas");
            ht = int.Parse(Console.ReadLine());
            Console.WriteLine("Introduce las Horas Extras trabajadas trabajadas");
            he = int.Parse(Console.ReadLine());
            ht = ht * 50;
            he = he * 50;
            salario = he + ht;
            Console.WriteLine("EL Salaro del Empleado Administrativo es de $" + salario);
            Console.WriteLine();
        }
    }
    class Gerencia:Empleado
    {
        public override void CalculaSalario()
        {
            Console.WriteLine("\nGerencia");
            Console.WriteLine("Introduce las Horas del turno trabajadas");
            ht = int.Parse(Console.ReadLine());
            Console.WriteLine("Introduce las Horas Extras trabajadas trabajadas");
            he = int.Parse(Console.ReadLine());
            ht = ht * 120;
            he = he * 120;
            salario = he + ht;
            Console.WriteLine("EL Salaro del Empleado de Gerencia es de $" + salario);
            Console.WriteLine();
        }
    }
    class Dueños:Empleado
    {
        public override void CalculaSalario()
        {
            Console.WriteLine("\nDueño");
            Console.WriteLine("Introduce las Horas del turno trabajadas");
            ht = int.Parse(Console.ReadLine());
            Console.WriteLine("Introduce las Horas Extras trabajadas trabajadas");
            he = int.Parse(Console.ReadLine());
            ht = ht * 200;
            he = he * 200;
            salario = he + ht;
            Console.WriteLine("EL Salaro del Dueño es de $" + salario);
            Console.WriteLine();
        }
    }
}
