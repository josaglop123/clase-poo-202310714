﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace programa2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void rbtRectangulo_Click(object sender, EventArgs e)
        {
            txtAltura.Enabled = false;
            txtRadio.Enabled = false;
            txtLargo.Enabled = true;
            txtAncho.Enabled = true;

        }

        private void rbtCircunferencia_Click(object sender, EventArgs e)
        {
            txtAltura.Enabled = false;
            txtLargo.Enabled = false;
            txtAncho.Enabled = false;
            txtRadio.Enabled = true;
        }

        private void rbtTriangulo_Click(object sender, EventArgs e)
        {
            txtAltura.Enabled = true;
            txtLargo.Enabled = true;
            txtAncho.Enabled = false;
            txtRadio.Enabled = false;
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            Rectangulo obj = new Rectangulo();
            obj.largo = double.Parse(txtLargo.Text);
            obj.ancho = double.Parse(txtAncho.Text);
            obj.CalcularArea();
            MessageBox.Show("El Area es = " + obj.area);
            Circunferencia obj2 = new Circunferencia();
            obj2.largo = double.Parse(txtRadio.Text);
            obj2.CalcularArea();
            MessageBox.Show("El Area es = " + obj2.area);
            Rectangulo obj3 = new Rectangulo();
            obj3.largo = double.Parse(txtLargo.Text);
            obj3.altura = double.Parse(txtAltura.Text);
            obj3.CalcularArea();
            MessageBox.Show("El Area es = " + obj3.area);
        }
    }
}
