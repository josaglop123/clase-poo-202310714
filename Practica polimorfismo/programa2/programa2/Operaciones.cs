﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace programa2
{
    class Operaciones
    {
        public double largo = 0.0;
        public double ancho = 0.0;
        public double altura = 0.0;
        public double radio = 0.0;
        public double area = 0.0;
        public virtual void CalcularArea()
        {

        }
    }
    class Rectangulo:Operaciones
    {
        public override void CalcularArea()
        {
            area = largo * ancho;
        }
    }
    class Circunferencia : Operaciones
    {
        public override void CalcularArea()
        {
            area = Math.PI*(radio*radio);
        }
    }
    class Triangulo : Operaciones
    {
        public override void CalcularArea()
        {
            area = (largo*altura)/2 ;
        }
    }
}
