﻿namespace programa2
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxFiguras = new System.Windows.Forms.GroupBox();
            this.rbtRectangulo = new System.Windows.Forms.RadioButton();
            this.rbtCircunferencia = new System.Windows.Forms.RadioButton();
            this.rbtTriangulo = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtLargo = new System.Windows.Forms.TextBox();
            this.txtAncho = new System.Windows.Forms.TextBox();
            this.txtRadio = new System.Windows.Forms.TextBox();
            this.txtAltura = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.gbxFiguras.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxFiguras
            // 
            this.gbxFiguras.Controls.Add(this.rbtTriangulo);
            this.gbxFiguras.Controls.Add(this.rbtCircunferencia);
            this.gbxFiguras.Controls.Add(this.rbtRectangulo);
            this.gbxFiguras.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.gbxFiguras.Location = new System.Drawing.Point(35, 39);
            this.gbxFiguras.Name = "gbxFiguras";
            this.gbxFiguras.Size = new System.Drawing.Size(116, 100);
            this.gbxFiguras.TabIndex = 0;
            this.gbxFiguras.TabStop = false;
            this.gbxFiguras.Text = "Tipo de figura";
            // 
            // rbtRectangulo
            // 
            this.rbtRectangulo.AutoSize = true;
            this.rbtRectangulo.Location = new System.Drawing.Point(7, 20);
            this.rbtRectangulo.Name = "rbtRectangulo";
            this.rbtRectangulo.Size = new System.Drawing.Size(80, 17);
            this.rbtRectangulo.TabIndex = 0;
            this.rbtRectangulo.TabStop = true;
            this.rbtRectangulo.Text = "Rectangulo";
            this.rbtRectangulo.UseVisualStyleBackColor = true;
            this.rbtRectangulo.Click += new System.EventHandler(this.rbtRectangulo_Click);
            // 
            // rbtCircunferencia
            // 
            this.rbtCircunferencia.AutoSize = true;
            this.rbtCircunferencia.Location = new System.Drawing.Point(7, 44);
            this.rbtCircunferencia.Name = "rbtCircunferencia";
            this.rbtCircunferencia.Size = new System.Drawing.Size(96, 17);
            this.rbtCircunferencia.TabIndex = 1;
            this.rbtCircunferencia.TabStop = true;
            this.rbtCircunferencia.Text = "Circunferencia ";
            this.rbtCircunferencia.UseVisualStyleBackColor = true;
            this.rbtCircunferencia.Click += new System.EventHandler(this.rbtCircunferencia_Click);
            // 
            // rbtTriangulo
            // 
            this.rbtTriangulo.AutoSize = true;
            this.rbtTriangulo.Location = new System.Drawing.Point(7, 67);
            this.rbtTriangulo.Name = "rbtTriangulo";
            this.rbtTriangulo.Size = new System.Drawing.Size(72, 17);
            this.rbtTriangulo.TabIndex = 2;
            this.rbtTriangulo.TabStop = true;
            this.rbtTriangulo.Text = "Triangulo ";
            this.rbtTriangulo.UseVisualStyleBackColor = true;
            this.rbtTriangulo.Click += new System.EventHandler(this.rbtTriangulo_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtAltura);
            this.groupBox1.Controls.Add(this.txtRadio);
            this.groupBox1.Controls.Add(this.txtAncho);
            this.groupBox1.Controls.Add(this.txtLargo);
            this.groupBox1.Location = new System.Drawing.Point(35, 187);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(116, 154);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos de La Figura";
            // 
            // txtLargo
            // 
            this.txtLargo.Location = new System.Drawing.Point(53, 38);
            this.txtLargo.Name = "txtLargo";
            this.txtLargo.Size = new System.Drawing.Size(50, 20);
            this.txtLargo.TabIndex = 0;
            // 
            // txtAncho
            // 
            this.txtAncho.Location = new System.Drawing.Point(53, 61);
            this.txtAncho.Name = "txtAncho";
            this.txtAncho.Size = new System.Drawing.Size(50, 20);
            this.txtAncho.TabIndex = 1;
            // 
            // txtRadio
            // 
            this.txtRadio.Location = new System.Drawing.Point(53, 87);
            this.txtRadio.Name = "txtRadio";
            this.txtRadio.Size = new System.Drawing.Size(50, 20);
            this.txtRadio.TabIndex = 2;
            // 
            // txtAltura
            // 
            this.txtAltura.Location = new System.Drawing.Point(53, 113);
            this.txtAltura.Name = "txtAltura";
            this.txtAltura.Size = new System.Drawing.Size(50, 20);
            this.txtAltura.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Largo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Ancho";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Radio";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Altura";
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(261, 83);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(87, 72);
            this.btnCalcular.TabIndex = 2;
            this.btnCalcular.Text = "Calcular Area";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(261, 187);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(87, 58);
            this.btnSalir.TabIndex = 3;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(407, 363);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbxFiguras);
            this.Name = "Form1";
            this.Text = "Form1";
            this.gbxFiguras.ResumeLayout(false);
            this.gbxFiguras.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxFiguras;
        private System.Windows.Forms.RadioButton rbtTriangulo;
        private System.Windows.Forms.RadioButton rbtCircunferencia;
        private System.Windows.Forms.RadioButton rbtRectangulo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtAltura;
        private System.Windows.Forms.TextBox txtRadio;
        private System.Windows.Forms.TextBox txtAncho;
        private System.Windows.Forms.TextBox txtLargo;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.Button btnSalir;
    }
}

