﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen
{
    class Operaciones
    {
        private int numero = 0;
        public Operaciones(int num)//constructor
        {
            this.numero = num;
        }

        public double SinIva()//sin iva
        {
            double resultado = 0;
            switch (this.numero)
            {
                case 0:
                    resultado = 1300;
                    break;
                case 1:
                    resultado = 1800;
                    break;
                case 2:
                    resultado = 4200;
                    break;
                case 3:
                    resultado = 3800;
                    break;
                default:
                    resultado = 0;
                    break;
            }

            return resultado;
        }

        public double IVA()// iva
        {
            double resultado = 0;
            switch (this.numero)
            {
                case 0:
                    resultado = 1300 * .16;
                    break;
                case 1:
                    resultado = 1800 * .16;
                    break;
                case 2:
                    resultado = 4200 * .16;
                    break;
                case 3:
                    resultado = 3800 * .16;
                    break;
                default:
                    resultado = 0;
                    break;
            }

            return resultado;
        }



        public double ConIva()//con iva
        {
            double resultado = 0;
            switch (this.numero)
            {
                case 0:
                    resultado = (1300 * .16) + 1300;
                    break;
                case 1:
                    resultado = (1800 * .16) + 1800;
                    break;
                case 2:
                    resultado = (4200 * .16) + 4200;
                    break;
                case 3:
                    resultado = (3800 * .16) + 3800;
                    break;
                default:
                    resultado = 0;
                    break;
            }
            return resultado;
        }
    }
}
