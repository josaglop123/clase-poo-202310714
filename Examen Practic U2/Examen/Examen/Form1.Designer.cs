﻿namespace Examen
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblSiniva = new System.Windows.Forms.Label();
            this.lblIva = new System.Windows.Forms.Label();
            this.lblConiva = new System.Windows.Forms.Label();
            this.btnCalcula = new System.Windows.Forms.Button();
            this.cbxSelecciona = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(135, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(155, 28);
            this.label1.TabIndex = 1;
            this.label1.Text = "Calcula el IVA";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(44, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(158, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Selecciona tu producto ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(62, 215);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Precio sin IVA:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(114, 244);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "IVA:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(57, 275);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Precio con IVA:";
            // 
            // lblSiniva
            // 
            this.lblSiniva.AutoSize = true;
            this.lblSiniva.Location = new System.Drawing.Point(137, 215);
            this.lblSiniva.Name = "lblSiniva";
            this.lblSiniva.Size = new System.Drawing.Size(16, 13);
            this.lblSiniva.TabIndex = 6;
            this.lblSiniva.Text = "...";
            // 
            // lblIva
            // 
            this.lblIva.AutoSize = true;
            this.lblIva.Location = new System.Drawing.Point(137, 244);
            this.lblIva.Name = "lblIva";
            this.lblIva.Size = new System.Drawing.Size(16, 13);
            this.lblIva.TabIndex = 7;
            this.lblIva.Text = "...";
            this.lblIva.Click += new System.EventHandler(this.label7_Click);
            // 
            // lblConiva
            // 
            this.lblConiva.AutoSize = true;
            this.lblConiva.Location = new System.Drawing.Point(137, 275);
            this.lblConiva.Name = "lblConiva";
            this.lblConiva.Size = new System.Drawing.Size(16, 13);
            this.lblConiva.TabIndex = 8;
            this.lblConiva.Text = "...";
            // 
            // btnCalcula
            // 
            this.btnCalcula.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalcula.Location = new System.Drawing.Point(158, 127);
            this.btnCalcula.Name = "btnCalcula";
            this.btnCalcula.Size = new System.Drawing.Size(107, 36);
            this.btnCalcula.TabIndex = 9;
            this.btnCalcula.Text = "CALCULAR";
            this.btnCalcula.UseVisualStyleBackColor = true;
            this.btnCalcula.Click += new System.EventHandler(this.btnCalcula_Click);
            // 
            // cbxSelecciona
            // 
            this.cbxSelecciona.FormattingEnabled = true;
            this.cbxSelecciona.Items.AddRange(new object[] {
            "Gabinete Yeyian SHADOW 2200",
            "Fuente De Poder Cooler Master 600wts",
            "Tarjete de video GIGABYTE 1080ti",
            "Procesador RYZEN 5 3600"});
            this.cbxSelecciona.Location = new System.Drawing.Point(223, 60);
            this.cbxSelecciona.Name = "cbxSelecciona";
            this.cbxSelecciona.Size = new System.Drawing.Size(202, 21);
            this.cbxSelecciona.TabIndex = 10;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 390);
            this.Controls.Add(this.cbxSelecciona);
            this.Controls.Add(this.btnCalcula);
            this.Controls.Add(this.lblConiva);
            this.Controls.Add(this.lblIva);
            this.Controls.Add(this.lblSiniva);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "IVA";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblSiniva;
        private System.Windows.Forms.Label lblIva;
        private System.Windows.Forms.Label lblConiva;
        private System.Windows.Forms.Button btnCalcula;
        private System.Windows.Forms.ComboBox cbxSelecciona;
    }
}

