﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Examen
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void btnCalcula_Click(object sender, EventArgs e)
        {
            int indice = cbxSelecciona.SelectedIndex;
            Operaciones obj = new Operaciones(indice);
            lblSiniva.Text = Convert.ToString(obj.SinIva());
            lblIva.Text = Convert.ToString(obj.IVA());
            lblConiva.Text = Convert.ToString(obj.ConIva());
        }
    }
}
