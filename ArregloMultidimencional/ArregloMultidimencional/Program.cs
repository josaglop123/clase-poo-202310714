﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArregloMultidimencional
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] Matriz = new int[2,4];

            for(int i=0;i<2;i++)
            {
                for(int j=0;j<4;j++)
                {
                    Console.WriteLine("Ingresa valor[" + i + ", " + j + "] = ");
                    Matriz[i, j] = int.Parse(Console.ReadLine()); 
                }
            }


            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Console.Write("  " + Matriz[i, j]);
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
