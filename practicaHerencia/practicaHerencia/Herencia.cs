﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practicaHerencia
{
    class Herencia
    {
        //Declaracion del atributo1 como tipo entero publico
        public int atributo1;
        //Declaracion del atributo2 como tipo entero privado
        private int atributo2;
        //Declaracion del atributo3 como tipo entero protected
        protected int atributo3;

        //Declaracion del metodo1 sin retorno de valor como tipo void publico
        public void metodo1()
        {
            //Impesion en pantalla 
            Console.WriteLine("Este es el metodo 1 de Herencia");
        }

        //Declaracion del metodo2 sin retorno de valor como tipo void privado
        private void metodo2()
        {
            //Impesion en pantalla 
            Console.WriteLine("Este Metodo no se puede acceder desde fuera de la clase Herencia");
        }

        //Declaracion del metodo3 sin retorno de valor como tipo void protected
        protected void metodo3()
        {
            //Impesion en pantalla 
            Console.WriteLine("Este es el metodo 3 de Herencia Protected");
        }

        //Declaracion del AccesoMetodo2 sin retorno de valor como tipo void publico
        public void AccesoMetodo2()
        {
            //Accediendo al metodo2
            metodo2();
        }
    }
    
    
    //Creacion de clase Hijo con herencia de la clase Herencia
    class Hijo: Herencia
    {
        //Declaracion del AccesoMetodo3 sin retorno de valor como tipo void publico
        public void AccesoMetodo3()
        {
            //Accediendo al meteodo3
            metodo3();
        }
    }
}
