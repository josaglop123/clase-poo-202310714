﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practicaHerencia
{
    class Program
    {
        static void Main(string[] args)
        {
            //Instanciasion de la clase Hijo en obj
            Hijo obj = new Hijo();
            //Refeencia al metodo1 para realizar las acciones dentro de el 
            obj.metodo1();
            //Refeencia al metodo3 para realizar las acciones dentro de el
            obj.AccesoMetodo3();
            
            //Instanciacion de la clase Herencia obj2
            Herencia obj2 = new Herencia();
            //Refeencia al metodo2 para realizar las acciones dentro de el
            obj2.AccesoMetodo2();

            //Esperar a que se precione una tecla
            Console.ReadKey();
        }
    }
}
