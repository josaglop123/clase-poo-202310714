﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog1_pracDes
{
    class Prueba
    {
        public Prueba(int x)
        {
            System.Console.Write("Creado objeto Prueba con x={0}", x);
        }

        ~Prueba()
        {
            Console.WriteLine("Out..");
        }
    }
}
