﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numprim
{
    class Primos
    {
        private int r=0;
        bool result;
        public Primos(int ran)
        { 
                this.r=ran;
        }
        public bool esPrimo(int r)
        {
            r = this.r;
            int divisor = 2;
            int resto = 0;
            if (divisor < r)
            {
                resto = r % divisor;
                if (resto == 0)
                {
                    return false;
                }
                
            }
            return true;
        }

        public void ImpriePrimos()
        {
            Console.WriteLine();
            Console.WriteLine("El {0} {1}", r, (result ? "es primo" : "no es primo"));
        }
        ~Primos()
        {
            Console.WriteLine("Out..");
        }
    }
}
