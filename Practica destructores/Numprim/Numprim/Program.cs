﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numprim
{
    class Program
    {
        static void Main(string[] args)
        {
            bool result;
            int numero;
            Console.WriteLine("Introduce un numero");
            numero = int.Parse(Console.ReadLine());
            Primos ob = new Primos(numero);
            result = ob.esPrimo(numero);
            Console.WriteLine("El {0} {1}", numero, (result ? "es primo" : "no es primo"));
            Console.ReadKey();
        }
    }
}
